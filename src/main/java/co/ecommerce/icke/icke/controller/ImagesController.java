package co.ecommerce.icke.icke.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.icke.icke.entity.Images;
import co.ecommerce.icke.icke.entity.Users;
import co.ecommerce.icke.icke.repository.ImagesRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/images")
public class ImagesController {

    @Autowired
    private ImagesRepository repoimages;

    @GetMapping
    public List<Images> all() {
        return repoimages.findAll();
    }

    @GetMapping("/{id}")
    public Images one(@PathVariable int id) {
        return repoimages.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Images add(@Valid @RequestBody Images images, @AuthenticationPrincipal Users users) {
        repoimages.save(images);
        return images;
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id);
        repoimages.deleteById(id);
    }
}