package co.ecommerce.icke.icke.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.icke.icke.entity.Categories;
import co.ecommerce.icke.icke.entity.Produits;
import co.ecommerce.icke.icke.entity.Users;
import co.ecommerce.icke.icke.repository.CategoriesRepository;
import co.ecommerce.icke.icke.repository.ProduitsRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/produits")
public class ProduitsController {

    @Autowired
    private ProduitsRepository repoProduits;
    @Autowired
    private CategoriesRepository repoCategories;

    @GetMapping
    public List<Produits> all() {
        return repoProduits.findAll();
    }

    @GetMapping("/{id}")
    public Produits one(@PathVariable int id) {
        return repoProduits.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produits add(@Valid @RequestBody Produits produits, @AuthenticationPrincipal Users users) {
        repoProduits.save(produits);
        return produits;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id);
        repoProduits.deleteById(id);
    }
/**
  @PutMapping("/{id}")
    public Produits update(@PathVariable int id, @Valid @RequestBody Produits pro, @AuthenticationPrincipal Users users) {
        Produits toUpdate = one(id);
        if(!toUpdate.getAuthor().getId().equals(users.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not your picture");
        }
        toUpdate.setImages(pro.getImages());
        toUpdate.setDescription(pro.getDescription());
        toUpdate.setTitle(pro.getTitle());
        repoProduits.save(toUpdate);
        return toUpdate;

    }

 */
       
    @GetMapping("/categories/{id}")
    public List<Produits> getProduitCategories(@PathVariable int id) {
        Categories categorie = repoCategories.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        return repoProduits.findByCategories(categorie);
    }

}
