package co.ecommerce.icke.icke.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.icke.icke.entity.Categories;
import co.ecommerce.icke.icke.entity.Users;
import co.ecommerce.icke.icke.repository.CategoriesRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/categories")
public class CategoriesController {

    @Autowired
    private CategoriesRepository repocategories;

    @GetMapping
    public List<Categories> all() {
        return repocategories.findAll();
    }

    @GetMapping("/{id}")
    public Categories one(@PathVariable int id) {
        return repocategories.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categories add(@Valid @RequestBody Categories categories, @AuthenticationPrincipal Users users) {
        repocategories.save(categories);
        return categories;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id);
        repocategories.deleteById(id);
    }
}
