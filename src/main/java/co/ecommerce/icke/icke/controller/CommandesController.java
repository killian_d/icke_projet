package co.ecommerce.icke.icke.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.icke.icke.entity.Commandes;
import co.ecommerce.icke.icke.repository.CommandesRepository;
import jakarta.validation.Valid;


@RestController
@RequestMapping("/api/commandes")
public class CommandesController {

    @Autowired
    private CommandesRepository repo;

    @GetMapping
    public List<Commandes> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Commandes one(@PathVariable int id) {
        return repo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Commandes add(@Valid @RequestBody Commandes commandes) {
        repo.save(commandes);
        return commandes;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        repo.delete(one(id));
    }

}
