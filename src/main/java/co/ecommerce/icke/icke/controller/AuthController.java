package co.ecommerce.icke.icke.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.icke.icke.entity.Users;
import co.ecommerce.icke.icke.repository.UsersRepository;
import jakarta.validation.Valid;

@RestController
public class AuthController {
    @Autowired
    private UsersRepository userRepo;
    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/api/account")
    public Users account(@AuthenticationPrincipal Users user) {
        return user;
    }

    @PostMapping("/api/user")
    @ResponseStatus(HttpStatus.CREATED)
    public Users register(@Valid @RequestBody Users user) {
        if(userRepo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }
        user.setRole("ROLE_USER");
        user.setPassword(encoder.encode(user.getPassword()));
        userRepo.save(user);
        return user;
    }

}
