package co.ecommerce.icke.icke.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.icke.icke.entity.Categories;
import co.ecommerce.icke.icke.entity.Produits;

@Repository
public interface ProduitsRepository extends JpaRepository<Produits, Integer> {

    List<Produits> findByCategories(Categories categorie);

}
