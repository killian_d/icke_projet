package co.ecommerce.icke.icke.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.icke.icke.entity.Commandes;

@Repository
public interface CommandesRepository extends JpaRepository<Commandes, Integer> {

}

