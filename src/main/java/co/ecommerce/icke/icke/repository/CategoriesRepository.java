package co.ecommerce.icke.icke.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.icke.icke.entity.Categories;

@Repository
public interface CategoriesRepository extends JpaRepository<Categories, Integer> {

}
