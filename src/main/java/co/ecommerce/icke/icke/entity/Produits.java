package co.ecommerce.icke.icke.entity;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
public class Produits {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Integer id;
    @NotBlank
    private String title;
    @Column(columnDefinition = "TEXT")
    private String description;
    @NotBlank
    private String size;
    private String color;
    @NotNull
    private Integer price;
    private String genre;
    private String marque;
    @NotNull
    @OneToMany(mappedBy = "produits")
    private List<Images> images = new ArrayList<>();
    @ManyToOne(fetch = FetchType.EAGER)
    private LineProduits lineproduits;
    @ManyToOne(fetch = FetchType.EAGER)
    private Categories categories;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public List<Images> getImages() {
        return images;
    }

    public void setImages(List<Images> images) {
        this.images = images;
    }

    public LineProduits getLineProduits() {
        return lineproduits;
    }

    public void setLineProduits(LineProduits lineProduits) {
        this.lineproduits = lineProduits;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public String getMainPicture() {
        if(images.size() > 0) {
            return images.get(0).getPicture();
        }
        return null;
    }

}
