package co.ecommerce.icke.icke.entity;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Commandes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String livraison;
    @JsonIgnore
    @OneToMany(mappedBy = "commandes")
    private List<LineProduits> lineproduits = new ArrayList<>();
    @ManyToOne(fetch = FetchType.EAGER)
    private Users author;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLivraison() {
        return livraison;
    }

    public void setLivraison(String livraison) {
        this.livraison = livraison;
    }

    public List<LineProduits> getLineproduits() {
        return lineproduits;
    }

    public void setLineproduits(List<LineProduits> lineproduits) {
        this.lineproduits = lineproduits;
    }

    public Users getAuthor() {
        return author;
    }

    public void setAuthor(Users author) {
        this.author = author;
    }




}
