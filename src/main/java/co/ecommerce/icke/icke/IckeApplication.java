package co.ecommerce.icke.icke;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IckeApplication {

	public static void main(String[] args) {
		SpringApplication.run(IckeApplication.class, args);
	}

}
