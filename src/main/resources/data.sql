-- Active: 1709906099311@@127.0.0.1@3306@ICKE
INSERT INTO users (adress, email, genre, name, password, role) VALUES
('19 rue pierre devvaux', 'admin@test.com', 'M', 'admin', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_ADMIN'),
('1 rue antoine devvaux', 'test@test.com', 'F', 'test', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER');


INSERT INTO categories (categorie) VALUES 
('haut'),
('bas'),
('chaussures');
INSERT INTO commandes (livraison,author_id) VALUES 
('19 rue pierre devaux',1),
('19 rue antoine devaux',2);

INSERT INTO line_produits (quantite,prix,commandes_id) VALUES 
(0,0,2),
(0,0,1), 
(0,0,2);


INSERT INTO produits (title,description,size,color,price,genre,marque,lineproduits_id,categories_id) VALUES 
('Veste','très bon produit','XL','vert',100,'M','Icke',2,1),
('Sweat','bon produit','L','bleue',80,'F','Nike',2,1),
('T-shirt','très bon produit','XL','orange',500,'M','Icke',2,1),
('T-shirt','bon produit','XL','marron',150,'M','Icke',2,1),
('T-shirt','très bon produit','XL','marron',130,'M','Icke',2,1),
('T-shirt','très bon produit','XL','rouge',130,'M','Icke',2,1),
('T-shirt','très bon produit','XL','bleue',130,'M','Icke',2,1),
('T-shirt','très bon produit','XL','blanc',130,'M','Icke',2,1),
('Pantalon','très bon produit','XL','beige',180,'M','Icke',2,2),
('Pantalon: Jean','bon produit','XL','bleue',200,'M','Nike',2,2),
('Pantalon: jogging','très bon produit','XL','noir',100,'M','Nike',2,2),
('Pantalon: jogging','bon produit','L','red',100,'noir','L.V',2,2),
('Pantalon','très bon produit','M','beige',1000,'M','Gucci',2,2),
('Pantalon: jogging','très bon produit','M','noir',800,'M','Icke',2,2),
('Pantalon: Jean','très bon produit','XL','bleu',700,'M','Icke',2,2),
('short','très bon produit','XXL','vert',1000,'M','Icke',2,2),
('Chaussure','très bon produit','XL','blanc',100,'M','Nike',2,3),
('Chaussure','bon produit','L','blanc-gris',1800,'F','Nike',2,3),
('Chaussure','très bon produit','XL','blanc',10,'M','New balance',2,3),
('Chaussure','bon produit','XL','noir-blanc',120,'M','Adidias',2,3),
('Chaussure','très bon produit','XL','noir',300,'M','Ye',2,3),
('Chaussure','très bon produit','XL','noir',1800,'M','Icke',2,3),
('Chaussure','très bon produit','XL','rouge',140,'M','Nike',2,3),
('Chaussure','très bon produit','XL','bleu',300,'M','Nike',2,3);




INSERT INTO images (picture, produits_id) VALUES 
('https://www.cdiscount.com/pdt2/3/9/1/3/550x550/mp113326391/rw/veste-homme-printemps-casual-veste-homme-col-monta.jpg',1),
('https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/4a1390a8-bb4d-498a-972a-2c1e340fee54/sweat-a-capuche-sportswear-club-pour-SLCXWt.png',2),
('https://i.pinimg.com/564x/02/88/cc/0288cc7de2a021babcdbbf6a4c9ea34a.jpg',3),
('https://i.pinimg.com/564x/91/4d/20/914d201aa3e785dccd7f1ce4ecfb993b.jpg',4),
('https://i.pinimg.com/564x/2d/b7/e7/2db7e707efc974dd88f2ee48d1b7c65b.jpg',5),
('https://i.pinimg.com/564x/57/5b/7d/575b7d9e9ff13cd1aec4107f678f824d.jpg',6),
('https://i.pinimg.com/564x/df/9e/18/df9e1880be3f9218545b48b8a3c3f2d2.jpg',7),
('https://i.pinimg.com/736x/65/24/5c/65245c3af235e5f639c836d137c0e7ee.jpg',8),
('https://i.pinimg.com/564x/bc/df/0f/bcdf0f49435fa85e9fb0b409510198d8.jpg',9),
('https://i.pinimg.com/564x/27/4f/88/274f881e8da544d3ec7fe04f37a9ea69.jpg',10),
('https://i.pinimg.com/564x/59/06/96/59069668138e9c8d98c0862565ea19df.jpg',11),
('https://i.pinimg.com/564x/d4/c9/06/d4c906034c5ece1e1859368a6154b207.jpg',12),
('https://i.pinimg.com/564x/a9/ed/45/a9ed453eac961484d8bfc34068789720.jpg',13),
('https://i.pinimg.com/564x/54/55/28/545528e76fac041b6ddc34a715e34f68.jpg',14),
('https://i.pinimg.com/564x/ae/a1/a6/aea1a692df01a06103141b119f067bc7.jpg',15),
('https://i.pinimg.com/564x/13/19/6d/13196d612e8776b2301e4d035ba93f97.jpg',16),
('https://i.pinimg.com/564x/2f/fe/85/2ffe85593265619f88431d38938a4445.jpg',17),
('https://i.pinimg.com/564x/48/c8/a7/48c8a727488d5fca8a66180abf13a84d.jpg',18),
('https://i.pinimg.com/564x/0c/dd/0b/0cdd0b0f906c41f576587443b69c7401.jpg',19),
('https://i.pinimg.com/564x/9e/64/f0/9e64f0eb54b5e45504420c83708706cd.jpg',20),
('https://laced.imgix.net/products/3bf31822-ab1e-43de-a950-9c45d94ef382.jpg?auto=format&fit=crop&w=1200',21),
('https://i.pinimg.com/564x/5b/8c/ef/5b8cef4f4cc964733e6018af2dbe4a0e.jpg',22),
('https://i.pinimg.com/564x/aa/62/09/aa6209d8703fbe58922142e14d244a4a.jpg',23),
('https://img01.ztat.net/article/spp-media-p1/dd7217ce1afb4aa2aba6c940ef844768/c66720860d7546b3813b1b98869843b1.jpg?imwidth=1800&filter=packshot',24);


/*('https://www.cdiscount.com/pdt2/3/9/1/3/550x550/mp113326391/rw/veste-homme-printemps-casual-veste-homme-col-monta.jpg',1),
('https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/4a1390a8-bb4d-498a-972a-2c1e340fee54/sweat-a-capuche-sportswear-club-pour-SLCXWt.png',2),
('https://i.pinimg.com/564x/02/88/cc/0288cc7de2a021babcdbbf6a4c9ea34a.jpg',3),
('https://i.pinimg.com/564x/91/4d/20/914d201aa3e785dccd7f1ce4ecfb993b.jpg',4),
('https://i.pinimg.com/564x/2d/b7/e7/2db7e707efc974dd88f2ee48d1b7c65b.jpg',5),
('https://i.pinimg.com/564x/39/a1/6f/39a16f77271e5b4a01635899890b153d.jpg',6),
('https://i.pinimg.com/564x/43/34/f7/4334f7e628ed398ba18b2e705550a876.jpg',7),
('https://i.pinimg.com/564x/60/d5/d4/60d5d4384f3a16ee48fc9e42e6635467.jpg',8),
('https://i.pinimg.com/564x/85/86/62/8586622be889c499d5ebaa5d775d1593.jpg',9),
('https://i.pinimg.com/564x/37/49/44/374944990191be1b5299e25d030235ad.jpg',10),
('https://i.pinimg.com/564x/bc/df/0f/bcdf0f49435fa85e9fb0b409510198d8.jpg',11),
('https://i.pinimg.com/564x/27/4f/88/274f881e8da544d3ec7fe04f37a9ea69.jpg',12),
('https://i.pinimg.com/564x/59/06/96/59069668138e9c8d98c0862565ea19df.jpg',13),
('https://i.pinimg.com/564x/d4/c9/06/d4c906034c5ece1e1859368a6154b207.jpg',14),
('https://i.pinimg.com/564x/13/19/6d/13196d612e8776b2301e4d035ba93f97.jpg',15),
('https://i.pinimg.com/564x/ae/6b/72/ae6b72a5bb49f918d13adab2d642b688.jpg',16),
('https://i.pinimg.com/564x/14/73/42/14734249e26a7d36e38f4effb9dbe9d9.jpg',17);

('https://i.pinimg.com/564x/a9/ed/45/a9ed453eac961484d8bfc34068789720.jpg',2),
('https://i.pinimg.com/564x/54/55/28/545528e76fac041b6ddc34a715e34f68.jpg',2),
('https://i.pinimg.com/564x/ae/a1/a6/aea1a692df01a06103141b119f067bc7.jpg',2),
('https://i.pinimg.com/564x/2f/fe/85/2ffe85593265619f88431d38938a4445.jpg',3),
('https://i.pinimg.com/564x/48/c8/a7/48c8a727488d5fca8a66180abf13a84d.jpg',3),
('https://i.pinimg.com/564x/0c/dd/0b/0cdd0b0f906c41f576587443b69c7401.jpg',3),
('https://i.pinimg.com/564x/9e/64/f0/9e64f0eb54b5e45504420c83708706cd.jpg',3),
('https://i.pinimg.com/564x/70/04/76/7004769a7e7ffa9d8fd54c18de1cfb48.jpg',3),
('https://i.pinimg.com/564x/5b/8c/ef/5b8cef4f4cc964733e6018af2dbe4a0e.jpg',3),
('https://i.pinimg.com/564x/02/2a/0a/022a0a653e3eafaa46b74a742783a347.jpg',3),
('https://i.pinimg.com/564x/aa/62/09/aa6209d8703fbe58922142e14d244a4a.jpg',3),
('https://i.pinimg.com/564x/ea/67/ea/ea67ea69c33aef169d9247108395515f.jpg',3),
('https://i.pinimg.com/564x/fd/c1/f4/fdc1f4e2f109c7ba58187c756d64578a.jpg',4);*/


